package m30
import groovy.xml.*

import groovy.json.JsonSlurper
import io.micronaut.context.annotation.Context
import io.micronaut.context.annotation.Value
import io.micronaut.http.client.multipart.MultipartBody
import io.micronaut.scheduling.annotation.Scheduled
import jakarta.inject.Singleton
import com.puravida.gif.GifGenerator
import javax.annotation.PostConstruct

@Singleton
@Context
class M30Listener {

    M30SseClient sseClient
    String token
    List<Camara> images = []
    M30Listener(M30SseClient sseClient,
                @Value('${mastodon.token}')Optional<String> token,
                @Value('${mastodon.user}')Optional<String> user,
                @Value('${mastodon.password}')Optional<String> password) {
        this.sseClient = sseClient
        this.token = token.isPresent() ? "Bearer ${token.get()}" :
                "Basic "+("${user.get()}:${password.get()}".bytes.encodeBase64())
    }

    @PostConstruct
    void init(){

        def xml = new XmlParser().parse( 'http://www.mc30.es/components/com_hotspots/datos/camaras.xml' )

        images = []
        xml.Camara.each{ c->
            Camara camara = new Camara(
                    nombre: "${c.Nombre.text()}",
                    url: "http://${c.URL.text()}",
                    latitud: "${c.Posicion.Latitud.text()}" as float,
                    longitud : "${c.Posicion.Longitud.text()}" as float,
            )
            images.add camara
        }
        Camara.sort(images)

        sseClient.userNotifications(token)
                .repeat(()->true)
                .onErrorContinue( (e)-> {
                    println e.toString()})
                .subscribe( (str)->{
                    if( str.indexOf('"id"') == -1)
                        return
                    Map json = new JsonSlurper().parseText(str)

                    if( json.type != "mention") {
                        return
                    }
                    json = json.status

                    if( json.in_reply_to_id  ){
                        return
                    }
                    if( json.username == 'm30' ){
                        return
                    }
                    println json.mentions
                    if( !json.mentions.find{ it.username == 'm30'} ){
                        return
                    }
                    notification(json)
                }, {err->
                    println err.toString()
                })
    }

    @Scheduled(cron = "0 0/15 5-20 * * ?")
    void everyHour(){
        generateGif(640, 480)
        def (message, retenciones) = estadoActual()
        println "retenciones ahora mismo $retenciones"
        if( retenciones > 1)
            tootea("$message", "", retenciones > 0)
    }

    void notification(Map toot) {
        def (message, retenciones) = estadoActual()
        tootea("@$toot.account.acct $message", toot.id, retenciones > 0)
    }

    void tootea(String message, String inReply="", boolean fotos=false){
        try {
            def medias = []
            if( fotos) {
                def body = MultipartBody.builder()
                        .addPart("file", "${System.currentTimeMillis()}.mp4", generateGif())
                        .build()
                def media = sseClient.uploadFile(token, body)
                medias.add media.id
            }

            Toot answer = new Toot(
                    status: message,
                    in_reply_to_id: inReply,
                    media_ids: medias
            )
            println "contestando $answer.status"
            sseClient.tootea(token, answer)
        }catch(Exception e){
            println "error $e"
            if( inReply ) {
                Toot answer = new Toot(
                        status: "$account, me pillas en la ducha, ahora no puedo mirarlo",
                        in_reply_to_id: inReply
                )
                sseClient.tootea(token, answer)
            }
        }
    }

    def estadoActual(){
        def datosTrafico = new XmlParser().parse( 'http://www.mc30.es/images/xml/DatosTrafico.xml' )
        def velocidadM30 = datosTrafico.DatoGlobal.find{ it.Nombre.text() == 'velicidadMediaSuperfice' }.VALOR.text()
        def ret = "ahora mismo la velocidad en la M30 es de $velocidadM30 Km/h\n\n"
        datosTrafico.DatoTrafico.Retenciones.VALOR.take(5).each{
            if( ret.length() < 400 )
                ret += "${it.text()}\n\n"
        }
        [ret, datosTrafico.DatoTrafico.Retenciones.VALOR.size()]
    }

    byte[] lastGif

    byte[] generateGif(int w = 640, int h=480){
        def byteImages = images.collect{camara ->
            "$camara.url?time=${System.nanoTime()}".toURL().bytes
        }
        def gif = GifGenerator.composeBytes(byteImages, 1000, true,w, h)
        lastGif = gif
    }
}
