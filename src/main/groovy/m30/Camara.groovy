package m30

class Camara {
    String nombre
    String url
    Float latitud
    Float longitud

    static List<Camara> sort(List<Camara> list){
        Camara first = list.first()
        list.sort{ camara->
            metersTo( camara.latitud, camara.longitud, first.latitud, first.longitud)
        }
    }

    static float metersTo(float lat1, float lng1, float lat2, float lng2) {
        double radioTierra = 6371;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double sindLat = Math.sin(dLat / 2);
        double sindLng = Math.sin(dLng / 2);
        double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
        double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
        double meters = radioTierra * va2;
        meters as float;
    }
}
