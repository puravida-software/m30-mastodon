package m30

import io.micronaut.runtime.Micronaut
import groovy.transform.CompileStatic

@CompileStatic
class Application {
    static void main(String[] args) {
        System.setProperty("user.timezone", "Europe/Madrid");
        Micronaut.run(Application, args)
    }
}
