package m30

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.QueryValue

@Controller("/tootea")
class M30Controller {


    M30Listener m30Listener
    M30Controller(M30Listener m30Listener){
        this.m30Listener = m30Listener
    }

    @Get("/send")
    String tootea(@QueryValue java.util.Optional<Boolean> fotos,
                  @QueryValue Optional<String> reply,
                  @QueryValue Optional<String> acct){
        String message = m30Listener.estadoActual()[0]
        message = acct.orElse("")+ " $message"
        m30Listener.tootea(message, reply.orElse(""),fotos.orElse(false ))
    }


    @Get(uri = "/m30.gif", produces = MediaType.IMAGE_GIF)
    byte[] gif(){
        byte [] image = m30Listener.lastGif ?: m30Listener.generateGif(800,600)
        image
    }


    @Get(uri = "/estado", produces = MediaType.TEXT_PLAIN)
    String estado(){
        m30Listener.estadoActual()[0]
    }
}
