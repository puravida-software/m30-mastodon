package m30

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.multipart.MultipartBody
import reactor.core.publisher.Flux

@Client('https://${mastodon.instance}')
interface M30SseClient {

    @Get(value = "/api/v1/streaming/health", processes = MediaType.TEXT_EVENT_STREAM)
    Flux<String> healthEvents();

    @Get(value = "/api/v1/streaming/public", processes = MediaType.TEXT_EVENT_STREAM)
    Flux<String> statusesEvents();

    @Get(value = "/api/v1/streaming/direct", processes = MediaType.TEXT_EVENT_STREAM)
    Flux<String> directMessage(@Header String authorization);

    @Get(value = "/api/v1/streaming/user", processes = MediaType.TEXT_EVENT_STREAM)
    Flux<String> userNotifications(@Header String authorization);

    @Post('/api/v1/statuses')
    String tootea(@Header String authorization, @Body Toot toot)

    @Post(value="/api/v1/media", produces = MediaType.MULTIPART_FORM_DATA, consumes = MediaType.APPLICATION_JSON)
    Map uploadFile(@Header String authorization, @Body MultipartBody fileUpload)
}
